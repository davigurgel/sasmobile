# SasMobile
=============

O Projeto foi desenvolvido com React Native (0.61.5) utilizando react-native-cli (2.0.1) para sua instalação e as seguintes bibliotecas:

- eslint (6.8.0)
- babel-plugin-root-import (6.4.1)
- eslint-import-resolver-babel-plugin-root-import (1.1.1)
- styled-components (4.4.1)
- axios (0.19.0)
- redux (4.0.4)
- redux-saga (1.1.3)
- react-redux (7.1.3)
- react-navigation (4.0.10)
- react-native-screens (1.0.0-alpha.23)
- react-navigation-stack (1.10.3)
- react-native-reanimated (1.4.0)
- react-native-gesture-handler (1.5.2)
- moment (2.24.0)
- @react-native-community/async-storage (1.6.2)

-------

Para configurar o ambiente, veja a seção [**React Native CLI Quickstart**](https://facebook.github.io/react-native/docs/getting-started.html). Siga as instruções de acordo com o sistema operacional usado para desenvolver.

## React Native CLI

Depois de configurar tudo, instale a interface de linha de comando do *React Native*. Execute o seguinte comando no seu terminal:

```bash
npm install -g react-native-cli
```

## Rodando a aplicação no emulador

Primeiro, instale as dependências do projeto executando o comando:

```bash
yarn
```

### iOS

Antes, instale as dependências do projeto iOS no diretório `/ios` executando o comando:

```bash
pod install
```

Com o Xcode instalado, execute o comando na pasta do projeto e o React Native rodará automaticamente no simulador iOS:

```bash
yarn ios
```

### Android

Abra o Android Studio, clique em "Configure" > "AVD Manager". Crie um dispositivo virtual, caso não exista, e o inicie. Com o emulador iniciado execute o comando na pasta do projeto:

```bash
yarn android
```
