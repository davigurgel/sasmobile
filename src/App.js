import React from 'react'
import { Provider } from 'react-redux'
import { StatusBar } from 'react-native'
import { ThemeProvider } from 'styled-components'

import store from '~/state'
import Routes from '~/routes'
import { theme } from '~/utils/theme'

const App = () => (
  <ThemeProvider theme={theme}>
    <Provider store={store}>
      <StatusBar
        backgroundColor={theme.statusBar.background}
        barStyle={theme.statusBar.style}
      />
      <Routes />
    </Provider>
  </ThemeProvider>
)

export default App
