import styled from 'styled-components/native'

export const Container = styled.FlatList.attrs({
  contentContainerStyle: {
    paddingVertical: 25,
    paddingHorizontal: 20,
  }
})``

export const Item = styled.TouchableOpacity.attrs({
  activeOpacity: 0.9,
})`
  background-color: ${({ theme }) => theme.colors.white};
  border: 1px solid ${({ selected, theme }) => (
    selected ? theme.colors.selected : theme.colors.notSelected
  )};
  padding: 19px;
  margin: 16px;
  border-radius: 8px;
  ${({ theme }) => theme.button.boxShadow};
`

export const ItemLabel = styled.Text`
  color: ${({ theme }) => theme.colors.black};
  font-size: 14px;
`

export const Question = styled.View``

export const QuestionHeader = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 25px;
`

export const Number = styled.Text`
  color: ${({ theme }) => theme.colors.primary};
  font-size: 16px;
  font-weight: bold;
`

export const Level = styled.View`
  background-color: ${({ theme }) => theme.colors.grayBg};
  padding: 2px 12px;
  border-radius: 15px;
`

export const LevelLabel = styled.Text`
  color: ${({ theme }) => theme.colors.primary};
  font-size: 12px;
`

export const QuestionText = styled.Text`
  color: ${({ theme }) => theme.colors.black};
  font-size: 16px;
  line-height: 22px;
  letter-spacing: 0.2px;
  text-transform: capitalize;
  margin-bottom: 30px;
`

export const ButtonContainer = styled.View.attrs({
  elevation: 5,
})`
  position: absolute;
  left: 0;
  right: 0;
  bottom: 0;
  padding: 0 12%;
  height: 84px;
  background-color: ${({ theme }) => theme.colors.white};
  justify-content: center;
  box-shadow: 0 -1px 4px rgba(0, 0, 0, 0.25);
  z-index: 150;
`
