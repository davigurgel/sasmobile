import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import moment from 'moment'

import { actions } from '~/crud/actions'
import storage from '~/services/storage'
import {
  MainContainer,
  Loading,
  Button,
  Modal,
} from '~/Components'
import {
  Container,
  Item,
  ItemLabel,
  Question,
  QuestionHeader,
  Number,
  Level,
  LevelLabel,
  QuestionText,
  ButtonContainer,
} from './styles'

const mapStateToProps = ({ sasMobile }) => ({ sasMobile })
const mapDispatchToProps = dispatch => (
  bindActionCreators({ ...actions }, dispatch)
)

class Test extends Component {
  state = {
    difficulty: 'medium',
    questionNumber: 0,
    selectedAnswer: null,
    questionOptions: null,
    goToNextQuestion: false,
    showModal: false,
    modalInfo: {},
  }

  componentDidMount() {
    this.fetchQuestion()
  }

  componentDidUpdate(prevProps) {
    const { sasMobile: { question } } = this.props

    if (
      JSON.stringify(question) !== JSON.stringify(prevProps.sasMobile.question)
      && question !== null
    ) {
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({
        questionOptions: this.shuffle([
          question.correct_answer,
          ...question.incorrect_answers,
        ])
      })
    }
  }

  fetchQuestion = () => {
    const { navigation, getQuestion, sasMobile: { questions } } = this.props
    const category = navigation.getParam('categoryId')

    const testInfo = this.getTestInformation(
      category,
      questions,
    )

    this.setState(testInfo)

    getQuestion({
      category,
      difficulty: testInfo.difficulty
    })
  }

  getTestInformation = (category, questions) => {
    const filtered = questions.filter(item => (
      +item.category === +category
    ))

    const categoryQuestions = filtered.length ? filtered[0] : null

    return {
      questionNumber: categoryQuestions
        ? categoryQuestions.questions.length
        : 0,
      difficulty: categoryQuestions
        ? this.getDifficulty(categoryQuestions.questions)
        : 'medium'
    }
  }

  getDifficulty = questions => {
    const difficulties = ['easy', 'medium', 'hard']
    const length = questions ? questions.length : 0

    if (length < 2) {
      return length ? questions[length - 1].difficulty : 'medium'
    }

    let { difficulty } = questions[length - 1]

    if (questions[length - 1].isCorrect && questions[length - 2].isCorrect) {
      const index = difficulties.indexOf(questions[length - 1].difficulty)

      difficulty = difficulties[index === 2 ? index : index + 1]
    }

    if (!questions[length - 1].isCorrect && !questions[length - 2].isCorrect) {
      const index = difficulties.indexOf(questions[length - 1].difficulty)

      difficulty = difficulties[index === 0 ? index : index - 1]
    }

    return difficulty
  }

  shuffle = array => {
    const editArray = array
    let ctr = array.length
    let temp = null
    let index = null

    while (ctr > 0) {
      index = Math.floor(Math.random() * ctr)
      ctr--
      temp = editArray[ctr]
      editArray[ctr] = editArray[index]
      editArray[index] = temp
    }

    return editArray
  }

  getDifficultyText = difficulty => {
    switch (difficulty) {
      case 'easy':
        return 'Fácil'

      case 'hard':
        return 'Difícil'

      default:
        return 'Médio'
    }
  }

  sendAnswer = () => {
    const { sasMobile: { question } } = this.props
    const { selectedAnswer } = this.state

    let data = {}

    if (question.correct_answer === selectedAnswer) {
      data = {
        type: 'success',
        message: 'Você acertou!',
      }
    } else {
      data = {
        type: 'error',
        message: 'Você errou!',
      }
    }

    this.setState({
      goToNextQuestion: true,
      modalInfo: data,
      showModal: true,
    })
  }

  fetchNextQuestion = () => {
    const {
      sasMobile: {
        questions,
        question,
      },
      sendAllQuestions,
      navigation,
    } = this.props
    const { questionNumber, selectedAnswer } = this.state
    const categoryId = navigation.getParam('categoryId')

    let allQuestions = []

    if (questions.length > 0) {
      const hasInRedux = questions.filter(item => item.category === categoryId)

      if (hasInRedux.length) {
        allQuestions = questions.map(item => {
          if (item.category === categoryId) {
            return {
              ...item,
              updatedAt: moment().format('L LT'),
              questions: [
                ...item.questions,
                {
                  ...question,
                  isCorrect: question.correct_answer === selectedAnswer,
                }
              ],
            }
          }

          return item
        })
      } else {
        allQuestions = [
          ...questions,
          {
            category: categoryId,
            updatedAt: moment().format('L LT'),
            questions: [
              {
                ...question,
                isCorrect: question.correct_answer === selectedAnswer,
              },
            ],
          }
        ]
      }
    } else {
      allQuestions = [{
        category: categoryId,
        updatedAt: moment().format('L LT'),
        questions: [
          {
            ...question,
            isCorrect: question.correct_answer === selectedAnswer,
          },
        ],
      }]
    }

    this.setState({
      selectedAnswer: null,
      questionOptions: null,
      goToNextQuestion: false,
      showModal: false,
      modalInfo: {},
    })

    sendAllQuestions(allQuestions, {
      after: () => {
        storage.save(
          'sasMobile/questions',
          JSON.stringify(allQuestions),
        )
        if (questionNumber + 1 === 10) {
          navigation.replace('SummaryPage', {
            categoryId: navigation.getParam('categoryId'),
            categoryName: navigation.getParam('categoryName'),
          })
        } else {
          navigation.replace('TestPage', {
            categoryId: navigation.getParam('categoryId'),
            categoryName: navigation.getParam('categoryName'),
          })
        }
      }
    })
  }

  render() {
    const { sasMobile: { question, loading } } = this.props
    const {
      questionNumber,
      difficulty,
      selectedAnswer,
      questionOptions,
      goToNextQuestion,
      showModal,
      modalInfo,
    } = this.state

    const questionText = question ? question.question : ''

    return (
      <MainContainer>
        {
          loading
            ? (
              <Loading />
            )
            : (
              <Fragment>
                <Container
                  data={questionOptions}
                  keyExtractor={(item, index) => String(index)}
                  renderItem={({ item }) => (
                    <Item
                      onPress={() => {
                        this.setState({ selectedAnswer: item })
                      }}
                      selected={selectedAnswer === item}
                    >
                      <ItemLabel>{ item }</ItemLabel>
                    </Item>
                  )}
                  ListHeaderComponent={(
                    <Question>
                      <QuestionHeader>
                        <Number>Questão { questionNumber + 1 }</Number>
                        <Level>
                          <LevelLabel>
                            { this.getDifficultyText(difficulty) }
                          </LevelLabel>
                        </Level>
                      </QuestionHeader>
                      <QuestionText>
                        { questionText }
                      </QuestionText>
                    </Question>
                  )}
                />
                {
                  selectedAnswer && (
                    <ButtonContainer>
                      {
                        goToNextQuestion ? (
                          <Button
                            onPress={() => this.fetchNextQuestion()}
                            label="Avançar"
                          />
                        ) : (
                          <Button
                            onPress={() => this.sendAnswer()}
                            label="Responder"
                          />
                        )
                      }
                    </ButtonContainer>
                  )
                }
                {
                  showModal && (
                    <Modal
                      type={modalInfo.type}
                      message={modalInfo.message}
                    />
                  )
                }
              </Fragment>
            )
        }
      </MainContainer>
    )
  }
}

Test.navigationOptions = ({ navigation }) => ({
  title: navigation.getParam('categoryName'),
})

export default connect(mapStateToProps, mapDispatchToProps)(Test)
