import React, { Component } from 'react'
import { RefreshControl } from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { actions } from '~/crud/actions'
import { theme } from '~/utils/theme'
import storage from '~/services/storage'
import { MainContainer, Loading } from '~/Components'
import {
  List,
  Label,
  Item,
  PageTitle,
  EmptyContent,
  EmptyContentTitle,
  EmptyContentSubtitle,
} from './styles'

const mapStateToProps = ({ sasMobile }) => ({ sasMobile })
const mapDispatchToProps = dispatch => (
  bindActionCreators({ ...actions }, dispatch)
)

class Home extends Component {
  state = {
    refreshing: false,
  }

  async componentDidMount() {
    const { setAllQuestions } = this.props
    const categoryStorage = JSON.parse(
      await storage.fetch('sasMobile/questions')
    )

    setAllQuestions(categoryStorage || [])

    this.refreshCategories()
  }

  refreshCategories = (refreshing = false) => {
    const { getCategories } = this.props

    if (refreshing) {
      this.setState(prevState => ({
        refreshing: !prevState.refreshing
      }))

      getCategories({
        after: () => this.setState(prevState => ({
          refreshing: !prevState.refreshing
        }))
      })
    } else {
      getCategories()
    }
  }

  testMyKnowledge = async category => {
    const { navigation, sasMobile: { questions } } = this.props
    const hasCategory = questions.length
      ? questions.filter(item => (
        item.category === category.id
      ))
      : questions
    const categoryQuestions = hasCategory.length ? hasCategory[0].questions : 0

    if (hasCategory.length > 0 && categoryQuestions.length === 10) {
      navigation.navigate(
        'SummaryPage',
        {
          categoryId: category.id,
          categoryName: category.name.split(':').pop(),
        },
      )
    } else {
      navigation.navigate(
        'TestPage',
        {
          categoryId: category.id,
          categoryName: category.name.split(':').pop(),
        },
      )
    }
  }

  render() {
    const { sasMobile: { categories, loading } } = this.props
    const { refreshing } = this.state

    return (
      <MainContainer>
        {
          loading && !refreshing
            ? (
              <Loading />
            )
            : (
              <List
                data={categories}
                keyExtractor={item => String(item.id)}
                renderItem={({ item }) => (
                  <Item onPress={() => this.testMyKnowledge(item)}>
                    <Label>
                      { item.name.split(':').pop() }
                    </Label>
                  </Item>
                )}
                refreshControl={(
                  <RefreshControl
                    refreshing={refreshing}
                    onRefresh={() => this.refreshCategories(true)}
                    tintColor={theme.colors.primary}
                  />
                )}
                ListHeaderComponent={(
                  <PageTitle>Categorias</PageTitle>
                )}
                ListEmptyComponent={(
                  <EmptyContent>
                    <EmptyContentTitle>
                      Nenhuma categoria encontrada.
                    </EmptyContentTitle>
                    <EmptyContentSubtitle>
                      Puxe para recarregar
                    </EmptyContentSubtitle>
                  </EmptyContent>
                )}
              />
            )
        }
      </MainContainer>
    )
  }
}

Home.navigationOptions = {
  title: 'Dev Mobile',
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)

