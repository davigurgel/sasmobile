import styled from 'styled-components/native'

export const List = styled.FlatList.attrs({
  contentContainerStyle: {
    padding: 24,
  }
})``

export const Label = styled.Text`
  font-size: 16px;
  line-height: 22px;
  color: ${({ theme }) => theme.colors.secondary};
  font-weight: bold;
  text-align: center;
`

export const Item = styled.TouchableOpacity`
  align-items: center;
  background-color: ${({ theme }) => theme.colors.white};
  border: 1px solid ${({ theme }) => theme.button.border};
  border-radius: ${({ theme }) => theme.button.radius};
  margin-top: 8px;
  margin-bottom: 8px;
  align-items: stretch;
  padding: 11px 22px;
  ${({ theme }) => theme.button.boxShadow};
`

export const PageTitle = styled.Text`
  color: ${({ theme }) => theme.colors.primary};
  font-size: 22px;
  margin-bottom: 30px;
  font-weight: bold;
`

export const EmptyContent = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
`

export const EmptyContentTitle = styled.Text`
  color: ${({ theme }) => theme.colors.primary};
  font-size: 20px;
`

export const EmptyContentSubtitle = styled.Text`
  color: ${({ theme }) => theme.colors.primary};
  font-size: 14px;
`

