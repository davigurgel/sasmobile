import styled from 'styled-components/native'
import { Dimensions } from 'react-native'

const { width } = Dimensions.get('window')

export const SummaryContainer = styled.View`
  flex-grow: 1;
  background-color: ${({ theme }) => theme.colors.background};
`

export const ScrollArea = styled.ScrollView``

export const SummaryHeader = styled.View`
  padding: 50px;
`

export const Title = styled.Text`
  font-size: 38px;
  line-height: 38px;
  font-weight: bold;
  color: ${({ theme }) => theme.colors.black};
`

export const Subtitle = styled.Text`
  letter-spacing: 0.2px;
  font-size: 28px;
  color: ${({ theme }) => theme.colors.black};
`

export const MessageSection = styled.View`
  position: relative;
  padding: 0 34px;
`

export const Line = styled.View`
  position: absolute;
  top: 50%;
  width: ${width};
  border: 1px dashed ${({ theme }) => theme.colors.primary};
`

export const Message = styled.View`
  align-items: center;
  padding: 10px 8px;
  border: 1px solid ${({ theme }) => theme.colors.primary};
  border-radius: 5px;
  background-color: ${({ theme }) => theme.colors.background};
`

export const MessageText = styled.Text`
  font-weight: bold;
  text-align: center;
  color: ${({ theme }) => theme.colors.primary};
  font-size: 18px;
`

export const ScoreInfo = styled.View`
  margin: 60px;
  background-color: ${({ theme }) => theme.colors.grayBg};
  border-radius: 10px;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`

export const ScoreInfoSection = styled.View`
  align-items: center;
  padding: 10px 50px;
`

export const ScoreInfoNumber = styled.Text`
  font-size: 50px;
  font-weight: bold;
  line-height: 50px;
  color: ${({ theme }) => theme.colors.primary};
`

export const ScoreInfoText = styled.Text`
  font-size: 18px;
`

export const DetailInfoContent = styled.View`
  padding: 0 10px;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`

export const DetailInfo = styled.View`
  margin: 10px 20px;
`

export const Divider = styled.View`
  height: 52px;
  border: 1px solid ${({ theme }) => theme.button.border};
`

export const DetailInfoTitle = styled.Text`
  font-size: 20px;
  font-weight: bold;
  color: ${({ theme }) => theme.colors.primary};
`

export const DetailInfoText = styled.Text`
  font-size: 18px;
  color: ${({ theme }) => theme.colors.primary};
`

export const ButtonArea = styled.View`
  justify-content: center;
  margin-top: 50px;
  padding: 50px;
`
