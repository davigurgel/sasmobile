import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'

import { MainContainer, Button } from '~/Components'
import {
  SummaryContainer,
  ScrollArea,
  SummaryHeader,
  Title,
  Subtitle,
  MessageSection,
  Line,
  Message,
  MessageText,
  ScoreInfo,
  ScoreInfoSection,
  ScoreInfoNumber,
  ScoreInfoText,
  DetailInfoContent,
  DetailInfo,
  DetailInfoTitle,
  DetailInfoText,
  Divider,
  ButtonArea,
} from './styles'

const mapStateToProps = ({ sasMobile }) => ({ sasMobile })

const Summary = ({ navigation, sasMobile }) => {
  const [categoryId] = useState(navigation.getParam('categoryId'))
  const [summaryInfo, setSummaryInfo] = useState(null)

  useEffect(() => {
    if (summaryInfo === null) {
      const filtered = sasMobile.questions.filter(item => (
        item.category === categoryId
      ))
      const categoryQuestions = filtered.pop().questions

      const easyLevel = { corrects: 0, wrongs: 0 }
      const mediumLevel = { corrects: 0, wrongs: 0 }
      const hardLevel = { corrects: 0, wrongs: 0 }
      let totalCorrects = 0
      let totalWrongs = 0

      // eslint-disable-next-line array-callback-return
      categoryQuestions.map(item => {
        switch (item.difficulty) {
          case 'easy':
            if (item.isCorrect) {
              easyLevel.corrects += 1
            } else {
              easyLevel.wrongs += 1
            }
            break

          case 'hard':
            if (item.isCorrect) {
              hardLevel.corrects += 1
            } else {
              hardLevel.wrongs += 1
            }
            break

          default:
            if (item.isCorrect) {
              mediumLevel.corrects += 1
            } else {
              mediumLevel.wrongs += 1
            }
            break
        }

        if (item.isCorrect) {
          totalCorrects += 1
        } else {
          totalWrongs += 1
        }
      })

      setSummaryInfo({
        easyLevel,
        mediumLevel,
        hardLevel,
        totalCorrects,
        totalWrongs,
      })
    }
  }, [])

  return (
    <MainContainer>
      {
        summaryInfo !== null && (
          <SummaryContainer>
            <ScrollArea>
              <SummaryHeader>
                <Title>Parabéns!</Title>
                <Subtitle>Você finalizou o teste</Subtitle>
              </SummaryHeader>
              <MessageSection>
                <Line />
                <Message>
                  <MessageText>
                    Veja seu desempenho nas questões
                  </MessageText>
                </Message>
              </MessageSection>
              <ScoreInfo>
                <ScoreInfoSection>
                  <ScoreInfoNumber>
                    { summaryInfo.totalCorrects }
                  </ScoreInfoNumber>
                  <ScoreInfoText>acertos</ScoreInfoText>
                </ScoreInfoSection>
                <ScoreInfoSection>
                  <ScoreInfoNumber>
                    { summaryInfo.totalWrongs }
                  </ScoreInfoNumber>
                  <ScoreInfoText>erros</ScoreInfoText>
                </ScoreInfoSection>
              </ScoreInfo>
              <DetailInfoContent>
                <DetailInfo>
                  <DetailInfoTitle>Fácil</DetailInfoTitle>
                  <DetailInfoText>
                    { `Acertos: ${summaryInfo.easyLevel.corrects}` }
                  </DetailInfoText>
                  <DetailInfoText>
                    { `Erros: ${summaryInfo.easyLevel.wrongs}` }
                  </DetailInfoText>
                </DetailInfo>
                <Divider />
                <DetailInfo>
                  <DetailInfoTitle>Médio</DetailInfoTitle>
                  <DetailInfoText>
                    { `Acertos: ${summaryInfo.mediumLevel.corrects}` }
                  </DetailInfoText>
                  <DetailInfoText>
                    { `Erros: ${summaryInfo.mediumLevel.wrongs}` }
                  </DetailInfoText>
                </DetailInfo>
                <Divider />
                <DetailInfo>
                  <DetailInfoTitle>Difícil</DetailInfoTitle>
                  <DetailInfoText>
                    { `Acertos: ${summaryInfo.hardLevel.corrects}` }
                  </DetailInfoText>
                  <DetailInfoText>
                    { `Erros: ${summaryInfo.hardLevel.wrongs}` }
                  </DetailInfoText>
                </DetailInfo>
              </DetailInfoContent>
              <ButtonArea>
                <Button
                  onPress={() => navigation.replace('HomePage')}
                  label="Voltar ao início"
                />
              </ButtonArea>
            </ScrollArea>
          </SummaryContainer>
        )
      }
    </MainContainer>
  )
}

Summary.navigationOptions = ({ navigation }) => ({
  title: navigation.getParam('categoryName'),
})

export default connect(mapStateToProps)(Summary)
