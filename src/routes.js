import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'

import Home from '~/pages/Home/Home'
import Test from '~/pages/Test/Test'
import Summary from '~/pages/Summary/Summary'
import { theme } from '~/utils/theme'

const AppStackNavigator = createStackNavigator(
  {
    HomePage: Home,
    TestPage: Test,
    SummaryPage: Summary,
  },
  {
    initialRouteName: 'HomePage',
    defaultNavigationOptions: {
      headerLeft: null,
      headerStyle: {
        backgroundColor: theme.colors.primary,
        elevation: 0,
        shadowOpacity: 1,
        shadowColor: 'transparent',
        borderBottomWidth: 0,
      },
      headerTitleContainerStyle: {
        marginHorizontal: 4,
      },
      headerTintColor: theme.colors.white,
      gesturesEnabled: false,
    },
  },
)

const Routes = createAppContainer(AppStackNavigator)

export default Routes
