import {
  all, takeLatest, put, call,
} from 'redux-saga/effects'
import { Alert } from 'react-native'

import api from '~/services/api'
import { actions, types } from './actions'

const defaultErrorMessage = 'Não foi possível carregar os dados'

function* getCategoriesSaga(action) {
  const { meta } = action
  const { after } = meta || {}

  yield put(actions.setLoading(true))
  try {
    const {
      data: { trivia_categories: triviaCategories }
    } = yield call(api.get, '/api_category.php')

    yield put(actions.setCategories(triviaCategories))
  } catch (error) {
    Alert.alert(defaultErrorMessage)
  } finally {
    yield put(actions.setLoading(false))
    if (after) {
      after()
    }
  }
}

function* getQuestionSaga(action) {
  const { meta: { category, difficulty } } = action

  yield put(actions.setLoading(true))
  yield put(actions.setQuestion(null))
  try {
    const { data: { results } } = yield call(
      api.get,
      `/api.php?amount=1&type=multiple&difficulty=${
        difficulty
      }&category=${
        category
      }`
    )

    yield put(actions.setQuestion(results[0]))
  } catch (error) {
    Alert.alert(defaultErrorMessage)
  } finally {
    yield put(actions.setLoading(false))
  }
}

function* sendAllQuestionsSaga(action) {
  const { payload, meta } = action
  const { after } = meta || {}

  try {
    yield put(actions.setAllQuestions(payload))
  } catch (error) {
    console.error(error)
  } finally {
    if (after) {
      after()
    }
  }
}

export default function* () {
  yield all([
    takeLatest(types.GET_CATEGORIES, getCategoriesSaga),
    takeLatest(types.GET_QUESTION, getQuestionSaga),
    takeLatest(types.SEND_ALL_QUESTIONS, sendAllQuestionsSaga),
  ])
}
