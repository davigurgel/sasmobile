import { typeActions } from '~/state/util'

const { actions, types } = typeActions('sasMobile', {
  getCategories: (meta = null) => ({ meta }),
  setCategories: data => ({ payload: data }),
  getQuestion: meta => ({ meta }),
  setQuestion: data => ({ payload: data }),
  sendAllQuestions: (data, meta = null) => ({ payload: data, meta }),
  setAllQuestions: data => ({ payload: data }),
  setLoading: data => ({ payload: data }),
})

export {
  actions,
  types,
}
