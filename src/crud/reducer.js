import { types } from './actions'

const INITIAL_STATE = {
  loading: false,
  categories: null,
  question: null,
  questions: [],
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.SET_LOADING:
      return {
        ...state,
        loading: action.payload,
      }

    case types.SET_CATEGORIES:
      return {
        ...state,
        categories: action.payload,
      }

    case types.SET_QUESTION:
      return {
        ...state,
        question: action.payload,
      }

    case types.SET_ALL_QUESTIONS:
      return {
        ...state,
        questions: action.payload,
      }

    default:
      return state
  }
}
