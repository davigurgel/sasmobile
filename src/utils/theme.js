export const theme = {
  statusBar: {
    background: 'transparent',
    style: 'dark-content',
  },
  colors: {
    primary: '#343C58',
    selected: '#4D8AF0',
    secondary: '#78809A',
    grayBg: '#B0B0B0',
    background: '#E5E5E5',
    notSelected: '#E4E4E6',
    white: '#fff',
    black: '#000',
    success: '#32CB82',
    error: '#FF6660',
  },
  button: {
    radius: '10px',
    border: '#B8BED5',
    boxShadow: '0px 1px 2px rgba(0, 0, 0, 0.15);',
  },
  modal: {
    overlay: 'rgba(0, 0, 0, 0.5)',
  },
}
