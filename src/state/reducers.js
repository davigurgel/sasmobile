import { combineReducers } from 'redux'

import sasMobile from '~/crud/reducer'

export default combineReducers({
  sasMobile,
})
