import styled from 'styled-components/native'

export const ButtonArea = styled.TouchableOpacity.attrs({
  activeOpacity: 0.9,
})`
  flex-direction: row;
  height: 56px;
  align-items: center;
  justify-content: center;
  background-color: ${({ theme }) => theme.colors.selected};
  border-radius: 15px;
`

export const ButtonLabel = styled.Text`
  color: ${({ theme }) => theme.colors.white};
  font-size: 24px;
  font-weight: bold;
  margin: 0 12px;
`
