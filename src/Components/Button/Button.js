import React from 'react'

import {
  ButtonArea,
  ButtonLabel,
} from './styles'

const Button = ({
  onPress,
  label,
}) => (
  <ButtonArea onPress={onPress}>
    <ButtonLabel>{ label }</ButtonLabel>
  </ButtonArea>
)

export default Button
