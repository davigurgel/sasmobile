import styled from 'styled-components/native'

export const LoadingIndicator = styled.ActivityIndicator``

export const LoadingContainer = styled.View`
  flex: 1;
  justify-content: center;
`
