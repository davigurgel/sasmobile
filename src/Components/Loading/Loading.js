import React from 'react'

import { LoadingContainer, LoadingIndicator } from './styles'
import { theme } from '~/utils/theme'

const Loading = () => (
  <LoadingContainer>
    <LoadingIndicator size="large" color={theme.colors.primary} />
  </LoadingContainer>
)

export default Loading
