import React from 'react'

import {
  ModalContainer,
  ModalContent,
  ModalIcon,
  ModalMessage,
} from './styles'
import cross from '~/assets/cross.png'
import tick from '~/assets/tick.png'

const Modal = ({ type, message }) => (
  <ModalContainer>
    <ModalContent type={type}>
      <ModalIcon source={type === 'success' ? tick : cross} />
      <ModalMessage>
        { message }
      </ModalMessage>
    </ModalContent>
  </ModalContainer>
)

export default Modal
