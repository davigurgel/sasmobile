import styled from 'styled-components/native'
import { Dimensions } from 'react-native'

const { height } = Dimensions.get('window')

const actionBoxHeight = 84
const headerHeight = 56

export const ModalContainer = styled.View`
  height: ${height - actionBoxHeight - headerHeight};
  flex: 1;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: ${actionBoxHeight};
  background-color: ${({ theme }) => theme.modal.overlay};
  z-index: 100;
  align-items: center;
  justify-content: center;
`

export const ModalContent = styled.View`
  background-color: ${({ theme }) => theme.colors.white};
  border: 3px solid ${({ type, theme }) => (
    type === 'success'
      ? theme.colors.success
      : theme.colors.error
  )};
  border-radius: 8px;
  align-items: center;
  justify-content: center;
  padding: 45px;
`

export const ModalIcon = styled.Image`
  margin-bottom: 15px;
`

export const ModalMessage = styled.Text`
  color: ${({ theme }) => theme.colors.primary};
  font-size: 20px;
  font-weight: bold;
  text-align: center;
`
