import AsyncStorage from '@react-native-community/async-storage'

export default {
  fetch: AsyncStorage.getItem,
  save: AsyncStorage.setItem,
  remove: AsyncStorage.removeItem,
  clear: AsyncStorage.removeItem,
}
