import axios from 'axios'

const api = axios.create({
  baseURL: 'https://opentdb.com'
})

export default {
  get: api.get,
  post: api.post,
  put: api.put,
  patch: api.patch,
  delete: api.delete,
}
